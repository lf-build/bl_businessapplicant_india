﻿using MongoDB.Driver;
using System.Collections.Generic;

namespace LendFoundry.Business.Applicant.India.Persistence
{
    public static class Extentions
    {
        public static UpdateDefinition<TDocument> SetDictionary<TDocument, TKey, TValue>(this UpdateDefinition<TDocument> update, IDictionary<TKey, TValue> dictionary)
        {
            foreach (var field in dictionary)
            {
                update = update.Set(field.Key as string, field.Value);
            }
            return update;
        }
    }
}
