﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Date;
using LendFoundry.Security.Encryption;
using System.Linq;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using LendFoundry.Security.Identity;

namespace LendFoundry.Business.Applicant.India.Persistence
{
    public class ApplicantRepository : MongoRepository<IApplicant, Applicant>, IApplicantRepository
    {
        #region Constructors

        static ApplicantRepository()
        {
           
            BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(TimeBucket);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });
            BsonClassMap.RegisterClassMap<Address>(map =>
            {
                map.AutoMap();
                var type = typeof(Address);
                map.MapProperty(p => p.AddressType).SetSerializer(new EnumSerializer<AddressType>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<PhoneNumber>(map =>
            {
                map.AutoMap();
                var type = typeof(PhoneNumber);
                map.MapProperty(p => p.PhoneType).SetSerializer(new EnumSerializer<PhoneType>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
            BsonClassMap.RegisterClassMap<EmailAddress>(map =>
            {
                map.AutoMap();
                var type = typeof(EmailAddress);
                map.MapProperty(p => p.EmailType).SetSerializer(new EnumSerializer<EmailType>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<BankInformation>(map =>
            {
                map.AutoMap();
                var type = typeof(BankInformation);
                map.MapProperty(p => p.AccountType).SetSerializer(new EnumSerializer<AccountType>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });          
            BsonClassMap.RegisterClassMap<SocialLinksRequest>(map =>
            {
                map.AutoMap();
                var type = typeof(SocialLinksRequest);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

        }

        #endregion Constructors

        #region Public Methods

        public ApplicantRepository(Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration,IEncryptionService encrypterService)
            : base(tenantService, configuration, "applicants-india")
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(Applicant)))
            {
                BsonClassMap.RegisterClassMap<Applicant>(map =>
            {
                map.AutoMap();
                map.MapProperty(a => a.Addresses).SetIgnoreIfDefault(true);
                map.MapProperty(a => a.PrimaryEmail).SetIgnoreIfDefault(true);
                map.MapProperty(a => a.BusinessPhone).SetIgnoreIfDefault(true);
                map.MapProperty(a => a.Owners).SetIgnoreIfDefault(true);
                map.MapProperty(a => a.EmailAddresses).SetIgnoreIfDefault(true);
                map.MapProperty(a => a.BankInformation).SetIgnoreIfDefault(true);
                map.MapProperty(a => a.LoanPriority).SetIgnoreIfDefault(true);
                map.MapProperty(a => a.BusinessLocation).SetIgnoreIfDefault(true);
                map.MapMember(m => m.Gstin).SetSerializer(new BsonEncryptor<string,string>(encrypterService));            
                map.MapMember(m => m.TinPanServiceTaxAssesseeCode).SetSerializer(new BsonEncryptor<string,string>(encrypterService));            
                map.MapMember(m => m.CinLlpin).SetSerializer(new BsonEncryptor<string,string>(encrypterService));  
                var type = typeof(Applicant);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
            }

            if (!BsonClassMap.IsClassMapRegistered(typeof(Owner)))
            {
                BsonClassMap.RegisterClassMap<Owner>(map =>
                {
                    map.AutoMap();
                    map.MapMember(m => m.PanId).SetSerializer(new BsonEncryptor<string,string>(encrypterService));
                    map.MapMember(m => m.AadhaarId).SetSerializer(new BsonEncryptor<string,string>(encrypterService));
                    map.MapMember(m => m.DOB).SetSerializer(new BsonEncryptor<string, string>(encrypterService));
                    var type = typeof(Owner);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                });
            }
            if (!BsonClassMap.IsClassMapRegistered(typeof(ApplicantUserIdentity)))
            {
                BsonClassMap.RegisterClassMap<ApplicantUserIdentity>(map =>
                {
                    map.AutoMap();
                    var type = typeof(ApplicantUserIdentity);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                });
                BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IApplicantUserIdentity, ApplicantUserIdentity>());
            }

            CreateIndexIfNotExists("tenantid-applicantId", Builders<IApplicant>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Id));
            CreateIndexIfNotExists("tenantid-userid",Builders<IApplicant>.IndexKeys.Ascending(i => i.TenantId).Ascending(i=>i.UserId));

        }

        public async Task<IApplicant> GetByApplicantId(string applicantId)
        {
            ObjectId obj;
            var isValid = ObjectId.TryParse(applicantId, out obj);
            if (!isValid)
                throw new NotFoundException($"Applicant {applicantId} not found");

            return await Query.FirstOrDefaultAsync(applicant =>  applicant.Id == applicantId);
        }

        public async Task AssociateUser(string applicantId, string userId)
        {
            var applicant = await GetByApplicantId(applicantId);

            if (applicant == null)
                throw new NotFoundException($"Applicant {applicantId} not found");

            if (!string.IsNullOrEmpty(applicant.UserId))
            {
                throw new UserAlreadyAssociatedException($"Applicant {applicantId} already has associated user");
            }

            await Collection.UpdateOneAsync(Builders<IApplicant>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                     a.Id == applicantId), Builders<IApplicant>.Update.Set(a => a.UserId, userId));
        }

        public async Task<IApplicant> SetAddresses(string applicantId, List<IAddress> addresses)
        {
            var applicant = await GetByApplicantId(applicantId);

            if (applicant == null)
                throw new NotFoundException($"Applicant {applicantId} not found");

            var lstAddress = applicant.Addresses ?? new List<IAddress>();

            foreach (var address in addresses)
            {
                if (string.IsNullOrWhiteSpace(address.AddressId))
                {
                    address.AddressId = Guid.NewGuid().ToString("N");
                }
                else
                {
                    var currentAddress = lstAddress.FirstOrDefault(a => a.AddressId == address.AddressId);
                    if (currentAddress == null)
                        throw new NotFoundException($"Address  {address.AddressId} for Applicant {applicantId} not found");

                    lstAddress.Remove(currentAddress);
                }
                lstAddress.Add(address);
            }
            applicant.Addresses = lstAddress;
            await Collection.UpdateOneAsync(Builders<IApplicant>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                   a.Id == applicantId),
                Builders<IApplicant>.Update.Set(a => a.Addresses, lstAddress));
            return applicant;
        }

        // public async Task<IApplicant> SetPhoneNumbers(string applicantId, List<IPhoneNumber> phoneNumbers)
        // {
        //     var applicant = await GetByApplicantId(applicantId);

        //     if (applicant == null)
        //         throw new NotFoundException($"Applicant {applicantId} not found");

        //     var lstPhoneNumber = applicant.PhoneNumbers ?? new List<IPhoneNumber>();

        //     foreach (var phone in phoneNumbers)
        //     {
        //         if (string.IsNullOrWhiteSpace(phone.PhoneId))
        //         {
        //             phone.PhoneId = Guid.NewGuid().ToString("N");
        //         }
        //         else
        //         {
        //             var currentPhone = lstPhoneNumber.FirstOrDefault(a => a.PhoneId == phone.PhoneId);
        //             if (currentPhone == null)
        //                 throw new NotFoundException($"Phone Number {nameof(currentPhone.Phone)} for Applicant {applicantId} not found");
        //             lstPhoneNumber.Remove(currentPhone);
        //         }
        //         lstPhoneNumber.Add(phone);
        //     }
        //     applicant.PhoneNumbers = lstPhoneNumber;
        //     await Collection.UpdateOneAsync(Builders<IApplicant>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
        //                                                            a.Id == applicantId),
        //         Builders<IApplicant>.Update.Set(a => a.PhoneNumbers, lstPhoneNumber));
        //     return applicant;
        // }

        public async Task<IApplicant> SetOwners(string applicantId, List<IOwner> owners)
        {
            var applicant = await GetByApplicantId(applicantId);

            if (applicant == null)
                throw new NotFoundException($"Applicant {applicantId} not found");

            var lstOwnerDetail = applicant.Owners ?? new List<IOwner>();

            foreach (var owner in owners)
            {
                if (string.IsNullOrWhiteSpace(owner.OwnerId))
                {
                    owner.OwnerId = Guid.NewGuid().ToString("N");
                }
                else
                {
                    var currentEmployment = lstOwnerDetail.FirstOrDefault(a => a.OwnerId == owner.OwnerId);
                    if (currentEmployment == null)
                        throw new NotFoundException($"Employment {nameof(owner.FirstName)} for Applicant {applicantId} not found");
                    lstOwnerDetail.Remove(currentEmployment);
                }
                applicant.Owners = lstOwnerDetail;               
                lstOwnerDetail.Add(owner);
            }

            await Collection.UpdateOneAsync(Builders<IApplicant>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                   a.Id == applicantId),
                Builders<IApplicant>.Update.Set(a => a.Owners, lstOwnerDetail));
            return applicant;
        }

        public async Task<IApplicant> SetBanks(string applicantId, List<IBankInformation> banks)
        {
            var applicant = await GetByApplicantId(applicantId);

            if (applicant == null)
                throw new NotFoundException($"Applicant {applicantId} not found");

            var lstBank = applicant.BankInformation ?? new List<IBankInformation>();

            foreach (var bank in banks)
            {
                if (string.IsNullOrWhiteSpace(bank.BankId))
                {
                    bank.BankId = Guid.NewGuid().ToString("N");
                }
                else
                {
                    var currentBank = lstBank.FirstOrDefault(a => a.BankId == bank.BankId);
                    if (currentBank == null)
                        throw new NotFoundException($"Bank with account number {nameof(bank.AccountNumber)} for Applicant {applicantId} not found");
                    lstBank.Remove(currentBank);
                }
                lstBank.Add(bank);
            }
            applicant.BankInformation = lstBank;
            await Collection.UpdateOneAsync(Builders<IApplicant>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                   a.Id == applicantId),
                Builders<IApplicant>.Update.Set(a => a.BankInformation, lstBank));
            return applicant;
        }

        public async Task<IApplicant> SetEmailAddresses(string applicantId, List<IEmailAddress> emailAddresses)
        {
            var applicant = await GetByApplicantId(applicantId);

            if (applicant == null)
                throw new NotFoundException($"Applicant {applicantId} not found");

            var lstEmailAddress = applicant.EmailAddresses ?? new List<IEmailAddress>();

            foreach (var emailAddress in emailAddresses)
            {
                if (string.IsNullOrWhiteSpace(emailAddress.Id))
                {
                    emailAddress.Id = Guid.NewGuid().ToString("N");
                }
                else
                {
                    var currentEmail = lstEmailAddress.FirstOrDefault(a => a.Id == emailAddress.Id);
                    if (currentEmail == null)
                        throw new NotFoundException($"Email Address {nameof(emailAddress.Email)} for Applicant {applicantId} not found");
                    lstEmailAddress.Remove(currentEmail);
                }
                lstEmailAddress.Add(emailAddress);
                applicant.EmailAddresses = lstEmailAddress;
            }

            await Collection.UpdateOneAsync(Builders<IApplicant>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                   a.Id == applicantId),
                Builders<IApplicant>.Update.Set(a => a.EmailAddresses, lstEmailAddress));
            return applicant;
        }

        public async Task<IApplicant> UpdateApplicant(string applicantId, IUpdateApplicantRequest updateApplicantRequest)
        {           
            var applicant = await GetByApplicantId(applicantId);

            if (applicant == null)
                throw new NotFoundException($"Applicant {applicantId} not found");

            await Collection.UpdateOneAsync(Builders<IApplicant>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                          a.Id == applicantId),
                Builders<IApplicant>.Update.Set(a => a.LegalBusinessName, updateApplicantRequest.LegalBusinessName)
                .Set(a => a.DBA, updateApplicantRequest.DBA)
                .Set(a => a.Gstin, updateApplicantRequest.Gstin)
                .Set(a => a.BusinessType, updateApplicantRequest.BusinessType)
                .Set(a => a.BusinessWebsite, updateApplicantRequest.BusinessWebsite)
                .Set(a => a.PrimaryEmail, updateApplicantRequest.PrimaryEmail)
                .Set(a => a.BusinessPhone, updateApplicantRequest.BusinessPhone)
                .Set(a => a.PrimaryAddress, updateApplicantRequest.PrimaryAddress)
                .Set(a => a.SocialLinks, updateApplicantRequest.SocialLinks)
                .Set(a => a.BusinessStartDate, updateApplicantRequest.BusinessStartDate)
                .Set(a => a.PrimaryFax, updateApplicantRequest.PrimaryFax)
                .Set(a => a.Industry, updateApplicantRequest.Industry)
                .Set(a => a.PropertyType, updateApplicantRequest.PropertyType)
                .Set(a => a.EmailAddresses, updateApplicantRequest.EmailAddresses)
                .Set(a => a.Addresses, updateApplicantRequest.Addresses)
                .Set(a => a.Owners, updateApplicantRequest.Owners)
                .Set(a => a.LoanPriority, updateApplicantRequest.LoanPriority)
                .Set(a => a.BusinessLocation, updateApplicantRequest.BusinessLocation)
                .Set(a => a. Gstin, updateApplicantRequest.Gstin)
                .Set(a => a.LegalStatusOfBusiness, updateApplicantRequest.LegalStatusOfBusiness)
                .Set(a => a.GSTRegistrationDate, updateApplicantRequest.GSTRegistrationDate)
                .Set(a => a.AuthorizedSignatories, updateApplicantRequest.AuthorizedSignatories)
                .Set(a => a.IsPrimaryContactDetail, updateApplicantRequest.IsPrimaryContactDetail)
                .Set(a => a.TinPanServiceTaxAssesseeCode, updateApplicantRequest.TinPanServiceTaxAssesseeCode)
                .Set(a => a.CinLlpin, updateApplicantRequest.CinLlpin)
                .Set(a => a.TinPanServiceTaxAssesseeCode, updateApplicantRequest.TinPanServiceTaxAssesseeCode)
                .Set(a => a.whereToSell, updateApplicantRequest.whereToSell)
                .Set(a => a.TypeOfProductOrService, updateApplicantRequest.TypeOfProductOrService)
                .Set(a => a.RegisteredBusinessName, updateApplicantRequest.RegisteredBusinessName)
                .Set(a => a.StatusofRegistration, updateApplicantRequest.StatusofRegistration)
                .Set(a => a.NatureOfBusiness, updateApplicantRequest.NatureOfBusiness)
                .Set(a => a.UserIdentity, updateApplicantRequest.UserIdentity));

            applicant = await GetByApplicantId(applicantId);
            return applicant;
        }

        public async Task<List<IApplicant>> GetApplicantbyUserId(string userId)
        {
            ObjectId obj;
            var isValid = ObjectId.TryParse(userId, out obj);
            if (!isValid)
                throw new NotFoundException($"User {userId} not found");

            return await Query.Where(a => a.TenantId == TenantService.Current.Id && a.UserId == userId).ToListAsync();
        }

        public async Task UpdateFields(string applicantId,IDictionary<string,object> fields)
        {
            var filter = Builders<IApplicant>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                     a.Id == applicantId);
            var firstField=fields.FirstOrDefault();
            fields.Remove(firstField.Key);
            var update = Builders<IApplicant>.Update.Set(firstField.Key, firstField.Value);
            if (fields.Count > 0)
            {
                update = update.SetDictionary(fields);
            }
            await Collection.UpdateOneAsync(filter,update);
        }

         public async Task<IApplicant> GetApplicantByBorrowerId(string borrowerId)
        {
          
            if (string.IsNullOrEmpty(borrowerId))
                throw new NotFoundException($"Applicant {borrowerId} not found");

            return await Query.FirstOrDefaultAsync(a => a.TenantId == TenantService.Current.Id && a.BorrowerId.ToLower() == borrowerId.ToLower());
        }

        public async Task<IApplicant> GetByToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
                throw new ArgumentNullException(nameof(token));

            return await Query.FirstOrDefaultAsync(a => a.TenantId == TenantService.Current.Id && a.UserIdentity != null && a.UserIdentity.Token == token);
        }



        #endregion Public Methods
    }
}