﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Business.Applicant.India.Client
{
    public interface IApplicantServiceClientFactory
    {
        IApplicantService Create(ITokenReader reader);
    }
}
