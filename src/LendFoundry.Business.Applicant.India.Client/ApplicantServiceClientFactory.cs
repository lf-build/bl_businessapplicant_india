﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Business.Applicant.India.Client
{
    public class ApplicantServiceClientFactory : IApplicantServiceClientFactory
    {
        #region Constructor
        public ApplicantServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public ApplicantServiceClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        private int CachingExpirationInSeconds { get; }
        private Uri Uri { get; }
        #endregion

        #region Private Properties
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        #endregion

        #region Public Methods
        public IApplicantService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("business_applicant_india");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new ApplicantService(client);

        }
        #endregion

    }
}
