﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Identity;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
namespace LendFoundry.Business.Applicant.India.Api.Controllers
{
    /// <summary>
    /// Controller
    /// </summary>
    [Route("/")]
    public class ApplicantController : ExtendedController
    {
        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="applicantService"></param>
        public ApplicantController(IApplicantService applicantService)
        {
            if (applicantService == null)
                throw new ArgumentNullException(nameof(applicantService));

            ApplicantService = applicantService;
        }

        #endregion Constructors

        #region Private Properties

        private IApplicantService ApplicantService { get; }
        private static NoContentResult NoContentResult { get; } = new NoContentResult();

        #endregion Private Properties

        #region Public Methods

        /// <summary>
        ///
        /// </summary>
        /// <param name="applicantRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(IApplicant))]
        public async Task<IActionResult> Add([FromBody]ApplicantRequest applicantRequest)
        {
            try
            {
                return Ok(await ApplicantService.Add(applicantRequest));
            }
            catch (UsernameAlreadyExists exception)
            {
                return new ErrorResult(409, exception.Message);
            }
            catch (UnableToCreateUserException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
             catch (Exception exception)
            {
               return new ErrorResult(409,exception.Message);
            }
            //return await ExecuteAsync(async () => Ok(await ApplicantService.Add(applicantRequest)));
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="applicantId"></param>
        /// <returns></returns>
        [HttpGet("/{applicantId}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(IApplicant))]
        public async Task<IActionResult> Get(string applicantId)
        {
            return await ExecuteAsync(async () => Ok(await ApplicantService.Get(applicantId)));
        }

        /// <summary>
        /// UpdateApplicant
        /// </summary>
        /// <param name="applicantId"></param>
        /// <param name="applicantRequest"></param>
        /// <returns></returns>
        [HttpPut("/{applicantId}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(IApplicant))]
        public async Task<IActionResult> UpdateApplicant(string applicantId, [FromBody]UpdateApplicantRequest applicantRequest)
        {
            return Ok(await ApplicantService.UpdateApplicant(applicantId, applicantRequest));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="applicantId"></param>
        /// <returns></returns>
        [HttpDelete("/{applicantId}")]
        public async Task<IActionResult> Delete(string applicantId)
        {
            return await ExecuteAsync(async () => { await ApplicantService.Delete(applicantId); return NoContentResult; });
        }

        /// <summary>
        /// SetAddresses
        /// </summary>
        /// <param name="applicantId"></param>
        /// <param name="addresses"></param>
        /// <returns></returns>
        [HttpPut("/{applicantId}/addresses")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> SetAddresses(string applicantId, [FromBody]List<Address> addresses)
        {
            return await ExecuteAsync(async () => { await ApplicantService.SetAddresses(applicantId, new List<IAddress>(addresses)); return NoContentResult; });
        }

        /// <summary>
        /// SetEmailAddresses
        /// </summary>
        /// <param name="applicantId"></param>
        /// <param name="emailAddresses"></param>
        /// <returns></returns>
        [HttpPut("/{applicantId}/emailaddresses")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> SetEmailAddresses(string applicantId, [FromBody]List<EmailAddress> emailAddresses)
        {
            return await ExecuteAsync(async () => { await ApplicantService.SetEmailAddresses(applicantId, new List<IEmailAddress>(emailAddresses)); return NoContentResult; });
        }

        // [HttpPut("/{applicantId}/phonenumbers")]
        // [Consumes("application/json")]
        // [Produces("application/json")]
        // public async Task<IActionResult> SetPhoneNumbers(string applicantId, [FromBody]List<PhoneNumber> phoneNumbers)
        // {
        //     return await ExecuteAsync(async () => { await ApplicantService.SetPhoneNumbers(applicantId, new List<IPhoneNumber>(phoneNumbers)); return NoContentResult; });
        // }

        /// <summary>
        /// SetOwners
        /// </summary>
        /// <param name="applicantId"></param>
        /// <param name="owners"></param>
        /// <returns></returns>
        [HttpPut("/{applicantId}/owners")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> SetOwners(string applicantId, [FromBody]List<Owner> owners)
        {
            return await ExecuteAsync(async () => { await ApplicantService.SetOwner(applicantId, new List<IOwner>(owners)); return NoContentResult; });
        }

        /// <summary>
        /// SetBanks
        /// </summary>
        /// <param name="applicantId"></param>
        /// <param name="banks"></param>
        /// <returns></returns>
        [HttpPut("/{applicantId}/banks")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> SetBanks(string applicantId, [FromBody]List<BankInformation> banks)
        {
            return await ExecuteAsync(async () => { await ApplicantService.SetBanks(applicantId, new List<IBankInformation>(banks)); return NoContentResult; });
        }

        /// <summary>
        /// AssociateUser
        /// </summary>
        /// <param name="applicantId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpPut("/{applicantId}/associate/user/{userId}")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> AssociateUser(string applicantId, string userId)
        {
            return await ExecuteAsync(async () =>
            {
                try 
                {
                    { await ApplicantService.AssociateUser(applicantId, userId); return NoContentResult; }
                }
                catch (UserAlreadyAssociatedException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        /// <summary>
        /// GetApplicantbyUserId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("applicant/by/{userId}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(List<IApplicant>))]
        public async Task<IActionResult> GetApplicantbyUserId(string userId)
        {
            return await ExecuteAsync(async () => Ok(await ApplicantService.GetApplicantbyUserId(userId)));
        }

        /// <summary>
        /// UpdateFields
        /// </summary>
        /// <param name="applicantId"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        [HttpPut("{applicantId}/update/fields")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(IApplicant))]
        public async Task<IActionResult> UpdateFields(string applicantId, [FromBody]Dictionary<string, object> fields)
        {
            return await ExecuteAsync(async () => Ok(await ApplicantService.UpdateFields(applicantId, fields)));
        }

        /// <summary>
        /// SetPrimary
        /// </summary>
        /// <param name="applicantId"></param>
        /// <param name="ownerId"></param>
        /// <returns></returns>
        [HttpPut("/{applicantId}/{ownerId}")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> SetPrimary(string applicantId, string ownerId)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    { await ApplicantService.SetPrimary(applicantId, ownerId); return NoContentResult; }
                }
                catch (UserAlreadyAssociatedException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        /// <summary>
        /// GetPrimaryOwner
        /// </summary>
        /// <param name="applicantId"></param>
        /// <returns></returns>
        [HttpGet("{applicantId}/primary")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(IOwner))]
        public async Task<IActionResult> GetPrimaryOwner(string applicantId)
        {
            return await ExecuteAsync(async () => Ok(await ApplicantService.GetPrimaryOwner(applicantId)));
        }

        /// <summary>
        /// GetApplicantByBorrowerId
        /// </summary>
        /// <param name="borrowerId"></param>
        /// <returns></returns>
        [HttpGet("applicant/{borrowerId}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(IApplicant))]
        public async Task<IActionResult> GetApplicantByBorrowerId(string borrowerId)
        {
            return await ExecuteAsync(async () => Ok(await ApplicantService.GetApplicantByBorrowerId(borrowerId)));
        }


        /// <summary>
        /// Creates the identity token.
        /// </summary>
        /// <param name="borrowerId">The identifier.</param>
        /// <returns>Token</returns>
        [HttpPost("applicant/{borrowerId}/create/identity/token")]
        //[Consumes("application/json")]
        [ProducesResponseType(typeof(ApplicantUserIdentity), 200)]
        public async Task<IActionResult> CreateIdentityToken(string borrowerId)
        {
            return await ExecuteAsync(async() => Ok(await ApplicantService.CreateIdentityToken(borrowerId)));
        }

        /// <summary>
        /// Verifies the token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>True or False</returns>
        [HttpPost("applicant/token/{token}/verify")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(bool))]
        public async Task<IActionResult> VerifyToken(string token)
        {
            return await ExecuteAsync(async() => Ok(await ApplicantService.IsTokenValid(token)));
        }

         /// <summary>
        /// FindByTokenAndLast4PanNumber
        /// </summary>
        /// <param name="token"></param>
        /// <param name="panNumber"></param>
        /// <returns></returns>
        [HttpGet("applicant/{token}/{panNumber}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(IApplicant))]
        public async Task<IActionResult> FindByTokenAndLast4PanNumber(string token,string panNumber)
        {
            return await ExecuteAsync(async () => Ok(await ApplicantService.FindByTokenAndLast4PanNumber(token,panNumber)));
        }


        #endregion Public Methods
    }
}