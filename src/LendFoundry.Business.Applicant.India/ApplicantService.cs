﻿
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LendFoundry.NumberGenerator;
using LendFoundry.Security.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LendFoundry.Business.Applicant.India
{
    #region Constructor

    public class ApplicantService : IApplicantService
    {
        public ApplicantService(IApplicantRepository applicantRepository,IEventHubClient eventHubClient,IGeneratorService numberGenerator, ITenantTime tenantTime,
        IApplicantConfigurations configuration
        )
        {
            EventHubClient = eventHubClient;
            ApplicantRepository = applicantRepository;
            NumberGenerator=numberGenerator;
            TenantTime = tenantTime;
            Configuration = configuration;
        }

        #endregion Constructor

        #region Private Properties

        private IApplicantRepository ApplicantRepository { get; }
        private IEventHubClient EventHubClient { get; }
        private IGeneratorService NumberGenerator { get; }
        private ITenantTime TenantTime { get; }
        private IApplicantConfigurations Configuration {get;}



        #endregion Private Properties

        public async Task<IApplicant> Add(IApplicantRequest applicantRequest)
        {
            if (applicantRequest == null)
                throw new ArgumentNullException(nameof(applicantRequest));

            if (applicantRequest.Addresses != null)
            {
                foreach (var address in applicantRequest.Addresses)
                {
                    address.AddressId = string.IsNullOrWhiteSpace(address.AddressId) ? GenerateUniqueId() : address.AddressId;
                }
            }
            if (applicantRequest.EmailAddresses != null)
            {
                foreach (var email in applicantRequest.EmailAddresses)
                {
                    email.Id = string.IsNullOrWhiteSpace(email.Id) ? GenerateUniqueId() : email.Id;
                }
            }
            if (applicantRequest.BankInformation != null)
            {
                if (applicantRequest.BankInformation != null && applicantRequest.BankInformation.Any())
                {
                    foreach (var bank in applicantRequest.BankInformation)
                    {
                        bank.BankId = string.IsNullOrWhiteSpace(bank.BankId) ? GenerateUniqueId() : bank.BankId;
                    }
                }
            }
            if (applicantRequest.Owners != null && applicantRequest.Owners.Any())
            {
                foreach (var owner in applicantRequest.Owners)
                {
                    if (owner.IsPrimary.HasValue)
                    {
                        owner.IsPrimary = owner.IsPrimary.Value;
                    }
                    owner.OwnerId = string.IsNullOrWhiteSpace(owner.OwnerId) ? GenerateUniqueId() : owner.OwnerId;
                    if (owner.Addresses != null && owner.Addresses.Any())
                    {
                        foreach (var address in owner.Addresses)
                        {
                            address.AddressId = string.IsNullOrWhiteSpace(address.AddressId) ? GenerateUniqueId() : address.AddressId;
                        }
                    }
                    if (owner.PhoneNumbers != null && owner.PhoneNumbers.Any())
                    {
                        foreach (var phone in owner.PhoneNumbers)
                        {
                            phone.PhoneId = string.IsNullOrWhiteSpace(phone.PhoneId) ? GenerateUniqueId() : phone.PhoneId;
                        }
                    }
                    if (owner.EmailAddresses != null && owner.EmailAddresses.Any())
                    {
                        foreach (var email in owner.EmailAddresses)
                        {
                            email.Id = string.IsNullOrWhiteSpace(email.Id) ? GenerateUniqueId() : email.Id;
                        }
                    }
                }
            }
            if(!string.IsNullOrWhiteSpace(applicantRequest.BorrowerId))
            {
              var applicant= await ApplicantRepository.GetApplicantByBorrowerId(applicantRequest.BorrowerId);
              if(applicant!=null)
              {
                  throw new ArgumentException($"BorrowerId :- {applicantRequest.BorrowerId} already exist");
              }
            }else
            {
              applicantRequest.BorrowerId=NumberGenerator.TakeNext ("applicant").Result;
            }
            IApplicant applicantModel = new Applicant(applicantRequest);           
            ApplicantRepository.Add(applicantModel);
            applicantRequest.Id = applicantModel.Id;
            await EventHubClient.Publish(new ApplicantCreated() { Applicant = applicantModel });
            return new Applicant(applicantRequest);
        }

        public async Task<IApplicant> Get(string applicantId)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicantId));

            var applicant = await ApplicantRepository.GetByApplicantId(applicantId);
            if (applicant == null)
                throw new NotFoundException($"Applicant {applicantId} not found");

            return applicant;
        }

        public async Task<IApplicant> UpdateApplicant(string applicantId, IUpdateApplicantRequest updateApplicantRequest)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentNullException(nameof(applicantId), "Value cannot be null or whitespace.");

            if (updateApplicantRequest.Addresses != null && updateApplicantRequest.Addresses.Any())
            {
                foreach (var address in updateApplicantRequest.Addresses)
                {
                    address.AddressId = string.IsNullOrWhiteSpace(address.AddressId) ? GenerateUniqueId() : address.AddressId;
                }
            }
           
            if (updateApplicantRequest.EmailAddresses != null && updateApplicantRequest.EmailAddresses.Any())
            {
                foreach (var email in updateApplicantRequest.EmailAddresses)
                {
                    email.Id = string.IsNullOrWhiteSpace(email.Id) ? GenerateUniqueId() : email.Id;
                }
            }
            if (updateApplicantRequest.Owners != null && updateApplicantRequest.Owners.Any())
            {
                foreach (var owner in updateApplicantRequest.Owners)
                {
                    if (owner.IsPrimary.HasValue)
                    {
                        owner.IsPrimary = owner.IsPrimary.Value;
                    }
                    owner.OwnerId = string.IsNullOrWhiteSpace(owner.OwnerId) ? GenerateUniqueId() : owner.OwnerId;
                    if (owner.Addresses != null && owner.Addresses.Any())
                    {
                        foreach (var address in owner.Addresses)
                        {
                            address.AddressId = string.IsNullOrWhiteSpace(address.AddressId) ? GenerateUniqueId() : address.AddressId;
                        }
                    }
                    if (owner.PhoneNumbers != null && owner.PhoneNumbers.Any())
                    {
                        foreach (var phone in owner.PhoneNumbers)
                        {
                            phone.PhoneId = string.IsNullOrWhiteSpace(phone.PhoneId) ? GenerateUniqueId() : phone.PhoneId;
                        }
                    }
                    if (owner.EmailAddresses != null && owner.EmailAddresses.Any())
                    {
                        foreach (var email in owner.EmailAddresses)
                        {
                            email.Id = string.IsNullOrWhiteSpace(email.Id) ? GenerateUniqueId() : email.Id;
                        }
                    }
                }
            }

            var applicant = await ApplicantRepository.UpdateApplicant(applicantId, updateApplicantRequest);
            await EventHubClient.Publish(new ApplicantModified() { Applicant = applicant });
            return applicant;
        }

        public async Task Delete(string applicantId)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicantId));

            var applicant = await ApplicantRepository.GetByApplicantId(applicantId);
            if (applicant == null)
                throw new NotFoundException($"Applicant {applicantId} not found");

            ApplicantRepository.Remove(applicant);
            await EventHubClient.Publish(new ApplicantDeleted() { Applicant = applicant });
        }

        public async Task AssociateUser(string applicantId, string userId)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicantId));

            if (string.IsNullOrWhiteSpace(userId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(userId));

            await ApplicantRepository.AssociateUser(applicantId, userId);
            await EventHubClient.Publish(new UserAssociatedWithApplicant() { ApplicantId = applicantId, UserId = userId });
        }

        public async Task SetAddresses(string applicantId, List<IAddress> addresses)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicantId));

            EnsureInputIsValid(addresses);

            var applicant = await ApplicantRepository.SetAddresses(applicantId, addresses);
            await EventHubClient.Publish(new ApplicantAddressModified() { Applicant = applicant });
        }

        // public async Task SetPhoneNumbers(string applicantId, List<IPhoneNumber> phoneNumbers)
        // {
        //     if (string.IsNullOrWhiteSpace(applicantId))
        //         throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicantId));

        //     EnsureInputIsValid(phoneNumbers);

        //     var applicant = await ApplicantRepository.SetPhoneNumbers(applicantId, phoneNumbers);
        //     await EventHubClient.Publish(new ApplicantPhoneNumberModified() { Applicant = applicant });
        // }

        public async Task SetEmailAddresses(string applicantId, List<IEmailAddress> emailAddresses)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentNullException(nameof(applicantId));

            EnsureInputIsValid(emailAddresses);
            var applicant = await ApplicantRepository.SetEmailAddresses(applicantId, emailAddresses);

            await EventHubClient.Publish(new ApplicantEmailModified() { Applicant = applicant });
        }

        public async Task SetOwner(string applicantId, List<IOwner> ownres)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicantId));

            if (ownres == null || ownres.Count <= 0)
                throw new ArgumentNullException(nameof(ownres));

            var applicant = await ApplicantRepository.SetOwners(applicantId, ownres);
            await EventHubClient.Publish(new ApplicantPhoneNumberModified() { Applicant = applicant });
        }

        public async Task SetPrimary(string applicantId, string ownerId)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicantId));

            if (string.IsNullOrWhiteSpace(ownerId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(ownerId));

            var applicant = await ApplicantRepository.GetByApplicantId(applicantId);
            if (applicant == null)
                throw new NotFoundException($"Applicant {applicantId} not found");
            if (applicant.Owners != null && applicant.Owners.Count > 0)
            {
                if (applicant.Owners.FirstOrDefault(item => item.OwnerId == ownerId) != null)
                {
                    foreach (var owner in applicant.Owners)
                    {
                        owner.IsPrimary = owner.OwnerId == ownerId;
                    }
                    await ApplicantRepository.SetOwners(applicantId, applicant.Owners.ToList());
                }
                else
                {
                    throw new NotFoundException($"Owner {ownerId} not found");
                }
            }
        }

        public async Task SetBanks(string applicantId, List<IBankInformation> banks)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentNullException(nameof(applicantId));

            if (banks == null || banks.Count <= 0)
                throw new ArgumentNullException(nameof(banks));

            EnsureInputIsValid(banks);

            var applicant = await ApplicantRepository.SetBanks(applicantId, banks);

            await EventHubClient.Publish(new ApplicantEmailModified() { Applicant = applicant });
        }

        public async Task<List<IApplicant>> GetApplicantbyUserId(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new ArgumentException("Argument is null or whitespace", nameof(userId));

            return await ApplicantRepository.GetApplicantbyUserId(userId);

        }

        public async Task<IApplicant> UpdateFields(string applicantId, IDictionary<string, object> fields)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentNullException(nameof(applicantId));

            if (fields == null || fields.Count == 0)
                throw new ArgumentNullException(nameof(fields));
            await ApplicantRepository.UpdateFields(applicantId, fields);
            return await Get(applicantId);
        }

        public async Task<IOwner> GetPrimaryOwner(string applicantId)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicantId));

            var applicant = await ApplicantRepository.Get(applicantId);
            if (applicant == null)
                throw new NotFoundException($"Applicant {applicantId} not found");

            if (applicant.Owners != null && applicant.Owners.Count > 0)
            {
                return applicant.Owners.FirstOrDefault(item => item.IsPrimary == true);
            }

            return null;
        }

         public async Task<IApplicant> GetApplicantByBorrowerId(string borrowerId)
        {
            if (string.IsNullOrWhiteSpace(borrowerId))
                throw new ArgumentException("Argument is null or whitespace", nameof(borrowerId));

            return await ApplicantRepository.GetApplicantByBorrowerId(borrowerId);

        }
       
       
        #region Private Methods - Validations
        private void EnsureInputIsValid(IList<IBankInformation> banks)
        {
            if (banks == null || !banks.Any())
                throw new ArgumentNullException($"Applicant {nameof(banks)} is mandatory");

            foreach (var bank in banks)
            {
                if (string.IsNullOrWhiteSpace(bank.BankName))
                    throw new ArgumentNullException($"{nameof(bank.BankName)} is mandatory");

                if (string.IsNullOrWhiteSpace(bank.AccountHolderName))
                    throw new ArgumentNullException($"{nameof(bank.AccountHolderName)} is mandatory");

                if (string.IsNullOrWhiteSpace(bank.AccountNumber))
                    throw new ArgumentNullException($"{nameof(bank.AccountNumber)} is mandatory");

                if (string.IsNullOrWhiteSpace(bank.IfscCode))
                    throw new ArgumentNullException($"{nameof(bank.IfscCode)} is mandatory");

                if (!Enum.IsDefined(typeof(AccountType), bank.AccountType))
                    throw new InvalidEnumArgumentException($"{nameof(bank.AccountType)} is not valid");
            }
        }

        private void EnsureInputIsValid(IList<IEmailAddress> emailAddresses)
        {
            if (emailAddresses == null || !emailAddresses.Any())
                throw new ArgumentNullException($"Applicant {nameof(emailAddresses)} is mandatory");

            foreach (var email in emailAddresses)
            {
                if (string.IsNullOrWhiteSpace(email.Email))
                    throw new ArgumentNullException($"{nameof(email.Email)} is mandatory");
                if (!Regex.IsMatch(email.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
                    throw new ArgumentException("Email Address is not valid");
                if (!Enum.IsDefined(typeof(EmailType), email.EmailType))
                    throw new InvalidEnumArgumentException($"{nameof(email.EmailType)} is not valid");
            }
        }

        private void EnsureInputIsValid(IList<IAddress> addresses)
        {
            if (addresses == null || !addresses.Any())
                throw new ArgumentNullException($"Applicant {nameof(addresses)} is mandatory");

            foreach (var address in addresses)
            {
                if (string.IsNullOrWhiteSpace(address.AddressLine1))
                    throw new ArgumentNullException($"Applicant {nameof(address.AddressLine1)} is mandatory");

                if (string.IsNullOrWhiteSpace(address.City))
                    throw new ArgumentNullException($"Applicant {nameof(address.City)} is mandatory");

                if (string.IsNullOrWhiteSpace(address.State))
                    throw new ArgumentNullException($"Applicant {nameof(address.State)} is mandatory");

                if (string.IsNullOrWhiteSpace(address.ZipCode))
                    throw new ArgumentNullException($"Applicant {nameof(address.ZipCode)} is mandatory");

                if (string.IsNullOrWhiteSpace(address.Country))
                    throw new ArgumentNullException($"Applicant {nameof(address.Country)} is mandatory");

                if (!Enum.IsDefined(typeof(AddressType), address.AddressType))
                    throw new InvalidEnumArgumentException($"{nameof(address.AddressType)} is not valid");

                if (!Enum.IsDefined(typeof(LocationType), address.LocationType))
                    throw new InvalidEnumArgumentException($"{nameof(address.LocationType)} is not valid");
            }
        }

        private void EnsureInputIsValid(IList<IPhoneNumber> phoneNumbers)
        {
            if (phoneNumbers == null || !phoneNumbers.Any())
                throw new ArgumentNullException($"Applicant {nameof(phoneNumbers)} is mandatory");

            foreach (var phone in phoneNumbers)
            {
                string phoneNumberPattern = "^(0|[1-9][0-9]+)$";
                if (!Regex.IsMatch(phone.Phone, phoneNumberPattern))
                    throw new ArgumentException("Invalid Phone Number format");
                if (string.IsNullOrWhiteSpace(phone.Phone))
                    throw new ArgumentNullException($"Applicant {nameof(phone.Phone)} is mandatory");

                if (!Enum.IsDefined(typeof(PhoneType), phone.PhoneType))
                    throw new InvalidEnumArgumentException($"{nameof(phone.PhoneType)} is not valid");
            }
        }

        private string GenerateUniqueId()
        {
            return Guid.NewGuid().ToString("N");
        }

        public async Task<bool> IsTokenValid(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
                throw new ArgumentException("Token cannot be empty.", nameof(token));

            var applicant = await ApplicantRepository.GetByToken(token);

            if(applicant != null && !string.IsNullOrWhiteSpace(applicant.UserId))
            {
                throw new ArgumentException($"User already available for borrower {applicant.BorrowerId}");
            }
            
            if (applicant?.UserIdentity != null)
            {
                if(applicant.UserIdentity.TokenExpiration !=null && !string.IsNullOrWhiteSpace(applicant.UserIdentity.Token))
                {
                  var now = TenantTime.Now;
                  var expiration = TenantTime.Convert(applicant.UserIdentity.TokenExpiration.Time);
                  Console.WriteLine($"{now} - {expiration}");
                  return expiration > now;
                }
                else if(!string.IsNullOrWhiteSpace(applicant.UserIdentity.Token))
                {
                    return true;
                }
            }
            return false;
        }

        public async Task<IApplicantUserIdentity> CreateIdentityToken(string borrowerId)
        {
            var identity = new ApplicantUserIdentity
            {
                Token = $"{Guid.NewGuid()}",
                TokenExpiration = Configuration.TokenExpirationInDays > 0 ? new TimeBucket(TenantTime.Now.AddDays(Configuration.TokenExpirationInDays)) : null 
            };
         
            var applicant = await ApplicantRepository.GetApplicantByBorrowerId(borrowerId);

            if (applicant == null || applicant.UserIdentity != null)
                throw new NotFoundException("Applicant not found.");
            applicant.UserIdentity = identity;
            await ApplicantRepository.UpdateApplicant(applicant.Id,CreateUpdateapplicantRequest(applicant));
            return applicant.UserIdentity;
        }

        public async Task<IApplicant> FindByTokenAndLast4PanNumber(string token, string last4Pan)
        {
            if (string.IsNullOrWhiteSpace(token))
                throw new ArgumentException("Argument is null or whitespace", nameof(token));

            if (string.IsNullOrWhiteSpace(last4Pan))
                throw new ArgumentException("Argument is null or whitespace", nameof(last4Pan));

            var applicant = await ApplicantRepository.GetByToken(token);

            var isValid = await IsTokenValid(token);

            if (applicant == null || !isValid)
                throw new ArgumentException($"Invalid token. Applicant not found or token expired.", nameof(token));

            if(!applicant.TinPanServiceTaxAssesseeCode.Substring(applicant.TinPanServiceTaxAssesseeCode.Length - 4, 4).Equals(last4Pan))
                throw new ArgumentException($"Invalid last four ssn digits", nameof(last4Pan));

            return applicant;
        }
         private IUpdateApplicantRequest CreateUpdateapplicantRequest(IApplicant applicant)
        {
            IUpdateApplicantRequest updateApplicantRequest = new UpdateApplicantRequest()
            {
               LegalBusinessName = applicant.LegalBusinessName,
               DBA = applicant.DBA,
               BusinessType = applicant.BusinessType,
               BusinessWebsite = applicant.BusinessWebsite,
               BusinessStartDate = applicant.BusinessStartDate,
               PrimaryFax = applicant.PrimaryFax,
               BusinessPhone = applicant.BusinessPhone,
               PrimaryEmail = applicant.PrimaryEmail,
               PrimaryAddress = applicant.PrimaryAddress,
               SocialLinks = applicant.SocialLinks,
               PropertyType = applicant.PropertyType,
               Owners = applicant.Owners,
               Industry = applicant.Industry,
               EmailAddresses = applicant.EmailAddresses,
               Addresses = applicant.Addresses,
               LoanPriority = applicant.LoanPriority,
               BusinessLocation = applicant.BusinessLocation,
               Gstin = applicant.Gstin,
               LegalStatusOfBusiness = applicant.LegalStatusOfBusiness,
               GSTRegistrationDate = applicant.GSTRegistrationDate,
               StatusofRegistration = applicant.StatusofRegistration,
               AuthorizedSignatories = applicant.AuthorizedSignatories,
               IsPrimaryContactDetail = applicant.IsPrimaryContactDetail,
               TinPanServiceTaxAssesseeCode = applicant.TinPanServiceTaxAssesseeCode,
               CinLlpin = applicant.CinLlpin,
               whereToSell = applicant.whereToSell,
               TypeOfProductOrService = applicant.TypeOfProductOrService,
               RegisteredBusinessName=applicant.RegisteredBusinessName,
               NatureOfBusiness=applicant.NatureOfBusiness,
               UserIdentity = applicant.UserIdentity,
            };

            return updateApplicantRequest;
        
        }


        #endregion Private Methods - Validations
    }
}