﻿namespace LendFoundry.Business.Applicant.India
{
    public class ApplicantDeleted
    {
        public IApplicant Applicant { get; set; }
    }
}
