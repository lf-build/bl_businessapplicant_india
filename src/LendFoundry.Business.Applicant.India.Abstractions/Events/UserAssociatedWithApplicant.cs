﻿namespace LendFoundry.Business.Applicant.India
{
    public class UserAssociatedWithApplicant
    {
        public string ApplicantId { get; set; }
        public string UserId { get; set; }
    }
}
