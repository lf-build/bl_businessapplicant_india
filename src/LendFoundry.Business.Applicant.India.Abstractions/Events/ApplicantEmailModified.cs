﻿namespace LendFoundry.Business.Applicant.India
{
    public class ApplicantEmailModified
    {
        public IApplicant Applicant { get; set; }
    }
}
