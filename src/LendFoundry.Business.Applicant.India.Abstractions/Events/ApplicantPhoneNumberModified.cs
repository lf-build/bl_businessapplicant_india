﻿namespace LendFoundry.Business.Applicant.India
{
    public class ApplicantPhoneNumberModified
    {
        public IApplicant Applicant { get; set; }
    }
}
