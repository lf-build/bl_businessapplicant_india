using LendFoundry.Foundation.Date;

namespace LendFoundry.Business.Applicant.India
{
    public interface IApplicantUserIdentity
    {
        string Token { get; set; }
        TimeBucket TokenExpiration { get; set; }
    }
}