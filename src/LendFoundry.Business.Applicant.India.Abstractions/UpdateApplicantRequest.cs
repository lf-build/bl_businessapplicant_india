﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;
using LendFoundry.Security.Identity;

namespace LendFoundry.Business.Applicant.India
{
    public class UpdateApplicantRequest : IUpdateApplicantRequest
    {
        #region Constructors
        public UpdateApplicantRequest() { }

        public UpdateApplicantRequest(IApplicantRequest applicantRequest)
        {
            LegalBusinessName = applicantRequest.LegalBusinessName;
            DBA = applicantRequest.DBA;
            BusinessType = applicantRequest.BusinessType;
            BusinessWebsite = applicantRequest.BusinessWebsite;
            BusinessStartDate = applicantRequest.BusinessStartDate;
            PrimaryFax = applicantRequest.PrimaryFax;
            BusinessPhone = applicantRequest.BusinessPhone;
            PrimaryEmail = applicantRequest.PrimaryEmail;
            PrimaryAddress = applicantRequest.PrimaryAddress;
            SocialLinks = applicantRequest.SocialLinks;
            PropertyType = applicantRequest.PropertyType;
            Owners = applicantRequest.Owners;
            Industry = applicantRequest.Industry;
            EmailAddresses = applicantRequest.EmailAddresses;
            Addresses = applicantRequest.Addresses;
            LoanPriority = applicantRequest.LoanPriority;
            BusinessLocation = applicantRequest.BusinessLocation;
            Gstin = applicantRequest.Gstin;
            LegalStatusOfBusiness = applicantRequest.LegalStatusOfBusiness;
            GSTRegistrationDate = applicantRequest.GSTRegistrationDate;
            StatusofRegistration = applicantRequest.StatusofRegistration;
            AuthorizedSignatories = applicantRequest.AuthorizedSignatories;
            IsPrimaryContactDetail = applicantRequest.IsPrimaryContactDetail;
            TinPanServiceTaxAssesseeCode = applicantRequest.TinPanServiceTaxAssesseeCode;
            CinLlpin = applicantRequest.CinLlpin;
            whereToSell = applicantRequest.whereToSell;
            TypeOfProductOrService = applicantRequest.TypeOfProductOrService;
            RegisteredBusinessName=applicantRequest.RegisteredBusinessName;
            NatureOfBusiness=applicantRequest.NatureOfBusiness;
            UserIdentity = applicantRequest.UserIdentity;
        }
        #endregion

        
        public string LegalBusinessName { get; set; }
        public string DBA { get; set; }
        public string BusinessType { get; set; }


        public string DUNSNumber { get; set; }

        public string BusinessWebsite { get; set; }

        public TimeBucket BusinessStartDate { get; set; }

        public string PrimaryFax { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneNumber, PhoneNumber>))]
        public IPhoneNumber BusinessPhone { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IEmailAddress, EmailAddress>))]
        public IEmailAddress PrimaryEmail { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress PrimaryAddress { get; set; }

        public SocialLinksRequest SocialLinks { get; set; }
        public string PropertyType { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IOwner, Owner>))]
        public IList<IOwner> Owners { get; set; }

        public string Industry { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IEmailAddress, EmailAddress>))]
        public IList<IEmailAddress> EmailAddresses { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public IList<IAddress> Addresses { get; set; }
        public string LoanPriority { get; set; }

        public string BusinessLocation { get; set; }
        public string Gstin { get; set; }
        public string LegalStatusOfBusiness { get; set; }
        public TimeBucket GSTRegistrationDate { get; set; }
        public string StatusofRegistration { get; set; }
        public List<string> AuthorizedSignatories { get; set; }
        public bool? IsPrimaryContactDetail { get; set; }
        public string TinPanServiceTaxAssesseeCode { get; set; }
        public string CinLlpin { get; set; }
        public string whereToSell { get; set; }
        public string TypeOfProductOrService { get; set; }
        public string RegisteredBusinessName { get; set; }
        public string NatureOfBusiness { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IApplicantUserIdentity, ApplicantUserIdentity>))]
        public IApplicantUserIdentity UserIdentity { get; set; }
    }
}