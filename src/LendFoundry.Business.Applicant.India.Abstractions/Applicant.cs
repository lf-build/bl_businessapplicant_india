﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;
using LendFoundry.Security.Identity;


namespace LendFoundry.Business.Applicant.India
{
    public class Applicant : Aggregate, IApplicant
    {
        #region Constructors

        public Applicant()
        {
        }

        public Applicant(IApplicantRequest applicant)
        {
            if (applicant == null)
                throw new ArgumentNullException(nameof(applicant));

            Id = applicant.Id;
            UserId = applicant.UserId;
            DBA = applicant.DBA;
            BusinessType = applicant.BusinessType;
            BusinessWebsite = applicant.BusinessWebsite;
            BusinessStartDate = applicant.BusinessStartDate;
            Owners = applicant.Owners;
            SocialLinks = applicant.SocialLinks;
            PrimaryFax = applicant.PrimaryFax;
            BusinessPhone = applicant.BusinessPhone;
            PrimaryEmail = applicant.PrimaryEmail;
            PrimaryAddress = applicant.PrimaryAddress;
            EmailAddresses = applicant.EmailAddresses;
            LegalBusinessName = applicant.LegalBusinessName;
            Addresses = applicant.Addresses;
            BankInformation = applicant.BankInformation;
            PropertyType = applicant.PropertyType;
            Industry = applicant.Industry;
            LoanPriority = applicant.LoanPriority;
            BusinessLocation = applicant.BusinessLocation;
            Gstin = applicant.Gstin;
            LegalStatusOfBusiness = applicant.LegalStatusOfBusiness;
            GSTRegistrationDate = applicant.GSTRegistrationDate;
            StatusofRegistration = applicant.StatusofRegistration;
            AuthorizedSignatories = applicant.AuthorizedSignatories;
            IsPrimaryContactDetail = applicant.IsPrimaryContactDetail;
            TinPanServiceTaxAssesseeCode = applicant.TinPanServiceTaxAssesseeCode;
            CinLlpin = applicant.CinLlpin;
            whereToSell = applicant.whereToSell;
            TypeOfProductOrService = applicant.TypeOfProductOrService;
            RegisteredBusinessName = applicant.RegisteredBusinessName;
            NatureOfBusiness = applicant.NatureOfBusiness;
            BorrowerId=applicant.BorrowerId;
            UserIdentity = applicant.UserIdentity;
        }

        #endregion Constructors

        #region Public Properties

        public string UserId { get; set; }
        public string LegalBusinessName { get; set; }
        public string DBA { get; set; }
        public string BusinessType { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IEmailAddress, EmailAddress>))]
        public IList<IEmailAddress> EmailAddresses { get; set; }

        // [JsonConverter(typeof(InterfaceListConverter<IPhoneNumber, PhoneNumber>))]
        // public IList<IPhoneNumber> PhoneNumbers { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public IList<IAddress> Addresses { get; set; }

        public string BusinessWebsite { get; set; }

        public TimeBucket BusinessStartDate { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBankInformation, BankInformation>))]
        public IList<IBankInformation> BankInformation { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IOwner, Owner>))]
        public IList<IOwner> Owners { get; set; }

        public SocialLinksRequest SocialLinks { get; set; }
        public string PrimaryFax { get; set; }

        public string Industry { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneNumber, PhoneNumber>))]
        public IPhoneNumber BusinessPhone { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IEmailAddress, EmailAddress>))]
        public IEmailAddress PrimaryEmail { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress PrimaryAddress { get; set; }

        public string PropertyType { get; set; }

        public string BusinessLocation { get; set; }

        public string LoanPriority { get; set; }

        public string Gstin { get; set; }
        public string LegalStatusOfBusiness { get; set; }
        public TimeBucket GSTRegistrationDate { get; set; }
        public string StatusofRegistration { get; set; }
        public List<string> AuthorizedSignatories { get; set; }
        public bool? IsPrimaryContactDetail { get; set; }
        public string TinPanServiceTaxAssesseeCode { get; set; }
        public string CinLlpin { get; set; }
        public string whereToSell { get; set; }
        public string TypeOfProductOrService { get; set; }
        public string RegisteredBusinessName { get; set; }
        public string NatureOfBusiness { get; set; }
        public string BorrowerId { get; set; }
        
        [JsonConverter(typeof(InterfaceConverter<IApplicantUserIdentity, ApplicantUserIdentity>))]
        public IApplicantUserIdentity UserIdentity { get; set; }
        


        #endregion Public Properties
    }
}