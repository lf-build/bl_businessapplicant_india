﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Business.Applicant.India
{
    public interface IApplicantRepository : IRepository<IApplicant>
    {

        Task<IApplicant> GetByApplicantId(string applicantId);

        Task<IApplicant> SetAddresses(string applicantId, List<IAddress> addresses);

      //  Task<IApplicant> SetPhoneNumbers(string applicantId, List<IPhoneNumber> phoneNumbers);

        Task<IApplicant> SetEmailAddresses(string applicantId, List<IEmailAddress> emailAddresses);

        Task<IApplicant> SetOwners(string applicantId, List<IOwner> owners);

        Task<IApplicant> SetBanks(string applicantId, List<IBankInformation> banks);

        Task<IApplicant> UpdateApplicant(string applicantId, IUpdateApplicantRequest updateApplicantRequest);

        Task AssociateUser(string applicantId, string userId);
        Task<List<IApplicant>> GetApplicantbyUserId(string userId);
        Task UpdateFields(string applicantId, IDictionary<string, object> fields);
        Task<IApplicant> GetApplicantByBorrowerId(string borrowerId);
        Task<IApplicant> GetByToken(string token);

    }
}
