﻿namespace LendFoundry.Business.Applicant.India
{
    public enum PhoneType
    {
        Residence,
        Mobile,
        Work,
        Alternate,
        Home,
        Others
    }
}