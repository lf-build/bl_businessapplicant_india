﻿namespace LendFoundry.Business.Applicant.India
{
    public enum AddressType
    {
        Home,
        Mailing,
        Business,
        Military,
        GST,
        Others
    }
}