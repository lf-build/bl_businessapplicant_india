namespace LendFoundry.Business.Applicant.India
{
    public interface IAddress
    {
        string AddressId { get; set; }
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
        string AddressLine3 { get; set; }
        string AddressLine4 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string ZipCode { get; set; }
        string Country { get; set; }
        AddressType AddressType { get; set; }
        bool IsDefault { get; set; }
        string LandMark { get; set; }
        LocationType LocationType { get; set; }

        string StreetNumber { get; set; }
        string StreetPreDirection { get; set; }
        string StreetName { get; set; }
        string StreetSuffix { get; set; }
        string UnitDesignation { get; set; }
        string UnitNumber { get; set; }
    }
}