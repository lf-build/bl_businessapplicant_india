﻿namespace LendFoundry.Business.Applicant.India
{
    public enum OwnershipType
    {
        General,
        Limited,
        JointVenture
    }
}