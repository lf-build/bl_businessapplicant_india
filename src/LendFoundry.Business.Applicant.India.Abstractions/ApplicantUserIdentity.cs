using LendFoundry.Foundation.Date;
namespace LendFoundry.Business.Applicant.India
{
    public class ApplicantUserIdentity : IApplicantUserIdentity
    {
        public string Token { get; set; }
        public TimeBucket TokenExpiration { get; set; }
    }
}