﻿using LendFoundry.Foundation.Client;
using System;

namespace LendFoundry.Business.Applicant.India
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "business-applicant";

    }
}
