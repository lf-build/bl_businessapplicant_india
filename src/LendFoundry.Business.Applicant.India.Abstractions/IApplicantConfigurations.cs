﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;
namespace LendFoundry.Business.Applicant.India
{
    public interface IApplicantConfigurations : IDependencyConfiguration
    {
        string[] DefaultRoles { get; set; }
        string ConnectionString { get; set; }
        int TokenExpirationInDays { get; set; }

    }
}