﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;
using LendFoundry.Security.Identity;

namespace LendFoundry.Business.Applicant.India
{
    public interface IApplicant : IAggregate
    {
        string UserId { get; set; }
        string LegalBusinessName { get; set; }
        string DBA { get; set; }
        string BusinessType { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IEmailAddress, EmailAddress>))]
        IList<IEmailAddress> EmailAddresses { get; set; }

        // [JsonConverter(typeof(InterfaceListConverter<IPhoneNumber, PhoneNumber>))]
        // IList<IPhoneNumber> PhoneNumbers { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        IList<IAddress> Addresses { get; set; } // Registered Address

        string BusinessWebsite { get; set; }

        TimeBucket BusinessStartDate { get; set; } // Business Established Date  

        [JsonConverter(typeof(InterfaceListConverter<IBankInformation, BankInformation>))]
        IList<IBankInformation> BankInformation { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IOwner, Owner>))]
        IList<IOwner> Owners { get; set; }

        string PrimaryFax { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneNumber, PhoneNumber>))]
        IPhoneNumber BusinessPhone { get; set; } // Business Contact Mobile

        [JsonConverter(typeof(InterfaceConverter<IEmailAddress, EmailAddress>))]
        IEmailAddress PrimaryEmail { get; set; } // Business Contact Email 

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        IAddress PrimaryAddress { get; set; }

        SocialLinksRequest SocialLinks { get; set; }
        string PropertyType { get; set; }
        string Industry { get; set; }

        string LoanPriority { get; set; }

        string BusinessLocation { get; set; }
        string Gstin { get; set; }
        string LegalStatusOfBusiness { get; set; }
        TimeBucket GSTRegistrationDate { get; set; }
        string StatusofRegistration { get; set; }
        List<string> AuthorizedSignatories {get;set;}
        bool?  IsPrimaryContactDetail{get;set;}
        string TinPanServiceTaxAssesseeCode{get;set;}
        string CinLlpin{get;set;}
        string whereToSell{get;set;}
        string TypeOfProductOrService{get;set;}
        string RegisteredBusinessName{get;set;}
        string NatureOfBusiness{get;set;}
        string BorrowerId { get; set; }
        
        [JsonConverter(typeof(InterfaceConverter<IApplicantUserIdentity, ApplicantUserIdentity>))]
        IApplicantUserIdentity UserIdentity { get; set; }
    }
}