﻿using System;

namespace LendFoundry.Business.Applicant.India
{
    public class BankInformation : IBankInformation
    {
        #region Public Properties
        public string BankId { get; set; }
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public string AccountHolderName { get; set; }
        public string IfscCode { get; set; }
        public AccountType AccountType { get; set; }
        public bool IsVerified { get; set; }
        public DateTimeOffset? VerifiedDate { get; set; }
        #endregion
    }
}
