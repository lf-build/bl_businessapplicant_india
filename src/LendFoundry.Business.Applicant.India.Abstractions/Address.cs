﻿namespace LendFoundry.Business.Applicant.India
{
    public class Address : IAddress
    {
        public string AddressId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string LandMark { get; set; }
        public LocationType LocationType { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public AddressType AddressType { get; set; }
        public bool IsDefault { get; set; }

        public string StreetNumber { get; set; }
        public string StreetPreDirection { get; set; }
        public string StreetName { get; set; }
        public string StreetSuffix { get; set; }
        public string UnitDesignation { get; set; }
        public string UnitNumber { get; set; }
    }
}