﻿namespace LendFoundry.Business.Applicant.India
{
    public enum EmailType
    {
        Personal,
        Work,
        Home,
        Others
    }
}