﻿namespace LendFoundry.Business.Applicant.India
{
    public interface IApplicantRequest :IApplicant
    {
        
        string ContactFirstName { get; set; }
        string ContactLastName { get; set; }

    }
}