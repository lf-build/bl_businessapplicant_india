﻿namespace LendFoundry.Business.Applicant.India
{
    public interface IEmailAddress
    {
        string Id { get; set; }
        string Email { get; set; }
        EmailType EmailType { get; set; }
        bool IsDefault { get; set; }
    }
}