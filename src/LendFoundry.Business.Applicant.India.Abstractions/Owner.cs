﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Business.Applicant.India
{
    public class Owner : IOwner
    {
        public string OwnerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DOB { get; set; }
        public OwnershipType OwnershipType { get; set; }
        public string OwnershipPercentage { get; set; }
        public string Designation { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IEmailAddress, EmailAddress>))]
        public IList<IEmailAddress> EmailAddresses { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPhoneNumber, PhoneNumber>))]
        public IList<IPhoneNumber> PhoneNumbers { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public IList<IAddress> Addresses { get; set; }
        public bool? IsPrimary { get; set; }
        public string AadhaarId { get; set; }
        public string PanId { get; set; }
    }
}