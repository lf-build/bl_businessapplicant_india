﻿namespace LendFoundry.Business.Applicant.India
{
    public class SocialLinksRequest
    {
        public string FacebookAddress { get; set; }
        public string YelpAddress { get; set; }
        public string GoogleAddress { get; set; }

    }
}