﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;
namespace LendFoundry.Business.Applicant.India
{
    public class ApplicantConfigurations : IApplicantConfigurations, IDependencyConfiguration
    {
        public string[] DefaultRoles { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
        public int TokenExpirationInDays { get; set; }

    }
}
