﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.Business.Applicant.India
{
    [Serializable]
    public class UserAlreadyAssociatedException : Exception
    {
        public UserAlreadyAssociatedException()
        {
        }

        public UserAlreadyAssociatedException(string message) : base(message)
        {
        }

        public UserAlreadyAssociatedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UserAlreadyAssociatedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}