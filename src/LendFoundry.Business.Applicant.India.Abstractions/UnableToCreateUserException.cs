﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.Business.Applicant.India
{
    [Serializable]
    public class UnableToCreateUserException : Exception
    {
        public UnableToCreateUserException()
        {
        }

        public UnableToCreateUserException(string message) : base(message)
        {
        }

        public UnableToCreateUserException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UnableToCreateUserException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}