﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;
using LendFoundry.Security.Identity;

namespace LendFoundry.Business.Applicant.India
{
    public interface IUpdateApplicantRequest
    {
        string LegalBusinessName { get; set; }
        string DBA { get; set; }
        string BusinessType { get; set; }

        string BusinessWebsite { get; set; }

        TimeBucket BusinessStartDate { get; set; }

        string PrimaryFax { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneNumber, PhoneNumber>))]
        IPhoneNumber BusinessPhone { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IEmailAddress, EmailAddress>))]
        IEmailAddress PrimaryEmail { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        IAddress PrimaryAddress { get; set; }

        SocialLinksRequest SocialLinks { get; set; }
        string PropertyType { get; set; }

        IList<IOwner> Owners { get; set; }

        string Industry { get; set; }


        IList<IEmailAddress> EmailAddresses { get; set; }

        IList<IAddress> Addresses { get; set; }

        string LoanPriority { get; set; }

        string BusinessLocation { get; set; }
        string Gstin { get; set; }
        string LegalStatusOfBusiness { get; set; }
        TimeBucket GSTRegistrationDate { get; set; }
        string StatusofRegistration { get; set; }
        List<string> AuthorizedSignatories { get; set; }
        bool? IsPrimaryContactDetail { get; set; }
        string TinPanServiceTaxAssesseeCode { get; set; }
        string CinLlpin { get; set; }
        string whereToSell { get; set; }
        string TypeOfProductOrService { get; set; }
        string RegisteredBusinessName { get; set; }
        string NatureOfBusiness { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IApplicantUserIdentity, ApplicantUserIdentity>))]
        IApplicantUserIdentity UserIdentity { get; set; }
    }
}